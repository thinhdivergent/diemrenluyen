-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 11 Avril 2015 à 18:29
-- Version du serveur :  5.6.21
-- Version de PHP :  5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `diemrenluyen`
--

-- --------------------------------------------------------

--
-- Structure de la table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
`log_id` int(10) unsigned NOT NULL,
  `log_tokens_id` int(10) unsigned NOT NULL,
  `log_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `tokens_id` int(2) NOT NULL DEFAULT '0',
  `tokens_token` varchar(10) DEFAULT NULL,
  `tokens_mssv` varchar(8) DEFAULT NULL,
  `tokens_hoten` varchar(28) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tokens`
--

INSERT INTO `tokens` (`tokens_id`, `tokens_token`, `tokens_mssv`, `tokens_hoten`) VALUES
(1, 'nk5evb7i3f', 'G1300124', 'Nguyễn Tường Anh'),
(2, 'pb1gnf7m84', '21300154', 'Trịnh Trung Anh'),
(3, 'rcirglhv1u', '51300280', 'Lê Vũ Bằng'),
(4, '0n4h8kac70', '41300782', 'Lê Tiến Đạt'),
(5, 'ds4tgutk28', '21300657', 'Lê Quang Dũng'),
(6, 'brbr1cdtsx', '21300571', 'Lê Trần Gia Duy'),
(7, '9bsvfee0yx', '41300607', 'Nguyễn Trọng Duy'),
(8, 'm5kvb2mkgx', 'V1300555', 'Đặng Bảo Duy'),
(9, 'qs4hqo7c8r', '41301047', 'Nguyễn Như Hải'),
(10, '5erqjad9m5', '41301164', 'Nguyễn Đình Hiếu'),
(11, 'xsq7xmdcdn', '21301378', 'Bùi Phạm Tâm Hòa'),
(12, 'qdtklfzf8n', '51301377', 'Bùi Minh Hòa'),
(13, 'pzxpg05sis', '41301360', 'Trần Đình Hoàng'),
(14, 't9fi7ec8ml', '61301368', 'Triệu Hoàng'),
(15, 'g8fu6jfmgv', 'V1301327', 'Lê Nguyễn Hoàng'),
(16, 'bn5hqhiotl', 'G1301415', 'Đinh Thị Bích Hợp'),
(17, '8kmmguoq6g', 'G1301640', 'Nguyễn Hòang Việt Hưng'),
(18, 'ba2avp310n', '41301643', 'Nguyễn Lê Hưng'),
(19, 'pwuz061xq1', '81301522', 'Phạm Xuân Huy'),
(20, 'c4gitckgxk', '81301961', 'Phạm Viết Anh Kiệt'),
(21, 'kjpyxxh004', '21301985', 'Trần Châu Kỳ'),
(22, '1hkdtvn6oj', '41302010', 'Lê Tùng Lâm'),
(23, 'ktk8f87h6w', '41302193', 'Lê Bá Lộc'),
(24, 'loygxk1rd6', '21302234', 'Phan Thành Lợi'),
(25, 'ybt508mmjk', '21302133', 'Lê Ngọc Bảo Long'),
(26, 'yytmu34op2', '21302141', 'Nguyễn Công Long'),
(27, 'hd78kz5fjs', 'G1302129', 'Đinh Hà Long'),
(28, '3ype6o63sl', '41302249', 'Nguyễn Hữu Luân'),
(29, 'kw1un7zt15', '41302332', 'Hồ Nhật Tường Minh'),
(30, 'u2yot0b661', '81302445', 'Nguyễn Hoàng Nam'),
(31, '7cv3lmog3i', '61302562', 'Nguyễn Tiến Nghị'),
(32, 'ks2hvs2rp7', '81302531', 'Nguyễn Hồ Nghĩa'),
(33, 'e8ci1fs018', '81302541', 'Nguyễn Trung Nghĩa'),
(34, 'vp0onqn6z8', '41302841', 'Ngô Minh Nhựt'),
(35, '0qy2cordfi', '91303527', 'Trần Thị Thanh Tâm'),
(36, 'bnkjbyen6m', '41303880', 'Hoàng Đình Thịnh'),
(37, '98rle9jdfk', '81304348', 'Nguyễn Công Trí'),
(38, '3rv1rogh6k', '21304675', 'Trần Xuân Tú'),
(39, 'ucx93byh0e', '81304561', 'Nguyễn Mạnh Tuấn'),
(40, 'ojo2a2hx2o', '51304731', 'Dương Hạ Uyên'),
(41, 'x89hgyn9q5', '41304752', 'Đường Thế Văn'),
(42, 'qrq45vvv2i', 'V1304851', 'Nguyễn Thịnh Phú Vinh'),
(43, 'v0usdskz21', '81300022', 'Nguyễn Minh An');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `log`
--
ALTER TABLE `log`
 ADD PRIMARY KEY (`log_id`), ADD KEY `log_tokens_id` (`log_tokens_id`);

--
-- Index pour la table `tokens`
--
ALTER TABLE `tokens`
 ADD PRIMARY KEY (`tokens_id`), ADD UNIQUE KEY `tokens_token` (`tokens_token`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `log`
--
ALTER TABLE `log`
MODIFY `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
